# RoadMap

## 2018

```mermaid
gantt
    title Roadmap 2018
    dateFormat  YY-MM
    section 2nd Album
    Drum Rec           :18-01, 18-02
    Other Rec		   :18-02, 18-04
    Release			   :18-06, 18-07
    section 3rd Album1
    SongWriting1 	   :18-01, 18-04
    Arrqnge1 		   :18-04, 18-10
    Drum Rec1		   :18-10, 18-11
    Other Rec1		   :18-11, 19-01
    Mix1			   :19-01, 19-03
    section 3rd Album2
    SongWriting2	   :18-07, 18-09
    Arrange2		   :18-09, 19-01
    Drum Rec2		   :19-01, 19-02
    Other Rec2		   :19-02, 19-04
    Mix2			   :19-04, 19-06
    section 3rd Album
    Jacket/ArtWork	   :18-08, 18-12
    Release			   :19-06, 19-07
    section Live
    2nd Live      :18-06  , 18-07
    3rd Live      :18-11  , 18-12
    section Session
    Yumi		  :18-08  , 18-09
```

